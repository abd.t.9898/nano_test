import 'package:dio/dio.dart';
import 'package:nano_test/core/networks/dio_helper.dart';
import 'package:nano_test/presentations/auth/login/bloc/login_event.dart';
import 'package:nano_test/utilities/api.dart';

class AuthHttp{
  DioHelper dioHelper;
  AuthHttp():dioHelper=DioHelper();

  Future<Response> loginAuthHttp({required PressLoginEvent event})async{
    try{
      Response response=await dioHelper.postFormData(
          data:
              {
                "username": event.email,/*"mor_2314",*/
                "password": event.password/*"83r5^_"*/
              },
          api: Api.login);
      return response;
    }catch(e){
      rethrow;
    }
  }
}