import 'package:dio/dio.dart';
import 'package:nano_test/core/networks/dio_helper.dart';
import 'package:nano_test/utilities/api.dart';

class HomeHttp{
  DioHelper dioHelper;
  HomeHttp():dioHelper=DioHelper();

  Future<Response> getProductsHomeHttp()async{
    try{
      Response response =await dioHelper.get(api: Api.getProducts);
      return response;
    }catch(e){
      rethrow;
    }
  }
  Future<Response> getProductHomeHttp({required int id}) async {
    Response response =await dioHelper.get(api: Api.getProduct(id: id));
    return response;
  }
}