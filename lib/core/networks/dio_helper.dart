import 'package:dio/dio.dart';
import 'package:localize_and_translate/localize_and_translate.dart';
import 'package:nano_test/utilities/injection_container.dart';

class DioHelper {
  Dio dio;

  DioHelper() : dio = myDio;

  Future<Response> postFormData({
    required Map<String,dynamic> data,
    required String api,
    Map<String, dynamic>? header,
    Map<String, dynamic>? parameter,
  }) async {
    try {
      dio.options.headers.clear();
      dio.options.queryParameters.clear();
      if (header != null) {
        dio.options.headers.addAll(header);
      }
      if (parameter != null) {
        dio.options.queryParameters.addAll(parameter);
      }
      dio.options.headers.addAll({"Accept": "application/json"});
      dio.options.headers.addAll({"lang": translator.activeLanguageCode});

      var response = await dio.postUri(Uri.parse(api), data: data);

      return response;
    } on DioError catch (e) {
      print(e.response);
      if (e.response?.statusCode == 403) {
        throw Exception(e.response);
      }else if (e.response?.statusCode == 401) {
        throw Exception(e.response);
      } else if (e.response?.statusCode == 400) {
        throw Exception(e.response);
      } else if (e.response?.statusCode == 500) {
        throw Exception(e.response?.statusMessage);
      } else if (e.type == DioErrorType.connectTimeout) {
        throw Exception('connectTimeout');
      } else if (e.type == DioErrorType.receiveTimeout) {
        throw Exception('receiveTimeout');
      } else if (e.type == DioErrorType.sendTimeout) {
        throw Exception('sendTimeout');
      } else if (e.type == DioErrorType.other) {
        throw Exception('internetConnectionError'.tr());
      }
      throw Exception('internetConnectionError'.tr());
    }
  }

  Future<Response> get({
    required String api,
    Map<String, dynamic>? header,
    Map<String, dynamic>? parameter,
  }) async {
    try {
      if (header != null) {
        dio.options.headers.clear();
        dio.options.headers.addAll(header);
      }
      if (parameter != null) {
        dio.options.queryParameters.clear();
        dio.options.queryParameters.addAll(parameter);
      }
      dio.options.headers.addAll({"Accept": "application/json"});
      dio.options.headers.addAll({"lang": translator.activeLanguageCode});

      var response = await dio.getUri(
        Uri.parse(api),
      );
      return response;
    } on DioError catch (e) {
      if (e.response?.statusCode == 403) {
        throw Exception(e.response!.statusMessage);
      } else if (e.response?.statusCode == 422) {
        Map<String, dynamic> res = e.response?.data['message'];
        String val = '';
        res.forEach((key, value) {
          val = "$val, ${value[0]}";
        });
        throw Exception(val);
      } else if (e.response?.statusCode == 401) {
        throw Exception(e.response!.statusMessage);
      } else if (e.response?.statusCode == 500) {
        throw Exception(e.response?.statusMessage);
      } else if (e.type == DioErrorType.connectTimeout) {
        throw Exception('connectTimeout');
      } else if (e.type == DioErrorType.receiveTimeout) {
        throw Exception('receiveTimeout');
      } else if (e.type == DioErrorType.sendTimeout) {
        throw Exception('sendTimeout');
      } else if (e.type == DioErrorType.other) {
        throw Exception('internetConnectionError'.tr());
      }
      throw Exception('internetConnectionError'.tr());
    }
  }
}
