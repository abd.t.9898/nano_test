class RatingModel {
  num rate;
  int count;

  RatingModel({
    required this.rate,
    required this.count,
  });

  RatingModel.fromJson(Map<String, dynamic> json)
      : rate = json['rate'] ?? 0.0,
        count = json['count'] ?? 0;
}
