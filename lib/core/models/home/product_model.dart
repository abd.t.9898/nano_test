import 'package:nano_test/core/models/home/rating_model.dart';

class ProductModel {
  int id;
  String title;
  num price;
  String description;
  String category;
  String image;
  RatingModel rating;

  ProductModel({
    required this.id,
    required this.title,
    required this.price,
    required this.description,
    required this.category,
    required this.image,
    required this.rating,
  });

  ProductModel.fromJson(Map<String, dynamic> json)
      : id = json['id'] ?? 0,
        title = json['title'] ?? '',
        description = json['description'] ?? '',
        category = json['category'] ?? '',
        image = json['image'] ?? '',
        price = json['price'] ?? 0.0,
        rating = RatingModel.fromJson(json['rating'] ?? {});

  static List<ProductModel> productsFromJson(List list) {
    List<ProductModel> products = [];
    for (var item in list) {
      products.add(ProductModel.fromJson(item));
    }
    return products;
  }
}
