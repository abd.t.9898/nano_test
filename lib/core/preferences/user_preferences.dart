import 'package:nano_test/utilities/injection_container.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserPreferences {
  SharedPreferences sharedPreferences;

  UserPreferences() : sharedPreferences = prefs;

  Future<void> setUser({required String token}) async {
    try {
      await sharedPreferences.setString('token', token);
    } catch (e) {
      throw Exception(e);
    }
  }

  Future<void> deleteUser() async {
    try {
      await sharedPreferences.remove('token');
    } catch (e) {
      throw Exception(e);
    }
  }

  String getUser() {
    try {
      String token = sharedPreferences.getString('token') ?? '';
      return token;
    } catch (e) {
      throw Exception(e);
    }
  }

  bool checkUserLoggedIn() {
    try {
      String token = getUser();

      if (token != '') {
        return true;
      }
      return false;
    } catch (e) {
      throw Exception(e);
    }
  }
}
