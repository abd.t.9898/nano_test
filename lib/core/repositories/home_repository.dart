import 'package:dio/dio.dart';
import 'package:nano_test/core/models/home/product_model.dart';

import '../networks/home_http.dart';

class HomeRepository {
  HomeHttp homeHttp;

  HomeRepository() : homeHttp = HomeHttp();

  Future<List<ProductModel>> getProductsHomeRepository() async {
    try {
      List<ProductModel> products = [];
      Response response = await homeHttp.getProductsHomeHttp();
      products = ProductModel.productsFromJson(response.data ?? []);
      return products;
    } catch (e) {
      rethrow;
    }
  }

  Future<ProductModel> getProductHomeRepository({required int id}) async {
    try {
      Response response = await homeHttp.getProductHomeHttp(id: id);
      ProductModel product = ProductModel.fromJson(response.data ?? {});
      return product;
    } catch (e) {
      rethrow;
    }
  }
}
