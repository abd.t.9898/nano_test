import 'package:dio/dio.dart';
import 'package:nano_test/core/networks/auth_http.dart';
import 'package:nano_test/core/preferences/user_preferences.dart';
import 'package:nano_test/presentations/auth/login/bloc/login_event.dart';

class AuthRepository {
  AuthHttp authHttp;
  UserPreferences userPreferences;

  AuthRepository()
      : authHttp = AuthHttp(),
        userPreferences = UserPreferences();

  Future<void> loginAuthRepository({required PressLoginEvent event}) async {
    try {
      Response response = await authHttp.loginAuthHttp(event: event);
      await userPreferences.setUser(token: response.data['token']);
    } catch (e) {
      rethrow;
    }
  }
}
