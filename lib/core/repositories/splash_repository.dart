import 'package:nano_test/core/preferences/user_preferences.dart';

class SplashRepository {
  UserPreferences userPreferences;

  SplashRepository() : userPreferences = UserPreferences();

  bool checkUserLoggedIn() {
    try {
      bool isLoggedIn = userPreferences.checkUserLoggedIn();
      return isLoggedIn;
    } catch (e) {
      rethrow;
    }
  }
}
