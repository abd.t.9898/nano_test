import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sizer/sizer.dart';
import '../../utilities/my_theme.dart';
import '../home/home_screen.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int index = 0;
  late PageController pageViewController;

  @override
  void initState() {
    pageViewController = PageController(initialPage: 0);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: PageView(
          controller: pageViewController,
          children:  const [
            HomeScreen(),
            Scaffold(),
            Scaffold(),
            Scaffold(),
          ],
          onPageChanged: (i) {
            setState(() {
              index = i;
            });
          },
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: [
          _barItem(
              title: 'home',
              path: 'assets/img/homeIcon.svg',
              isSelected: index == 0),
          _barItem(
              title: 'cart',
              path: 'assets/img/cartIcon.svg',
              isSelected: index == 1),
          _barItem(
              title: 'heart',
              path: 'assets/img/heartIcon.svg',
              isSelected: index == 2),
          // _barItem(title: 'Settings', path: 'assets/img/chart.svg',isSelected:index==3),
          _barItem(
              title: 'profile',
              path: 'assets/img/personIcon.svg',
              isSelected: index == 3),
        ],
        currentIndex: index,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        type: BottomNavigationBarType.fixed,
        //selectedItemColor: MyTheme.blueColor,
        backgroundColor: Colors.white,
        //unselectedItemColor: MyTheme.greyColor,
        onTap: (i) {
          pageViewController.jumpToPage(i);
          setState(() {
            index = i;
          });
        },
      ),
    );
  }

  BottomNavigationBarItem _barItem(
      {required String title, required String path, required bool isSelected}) {
    return BottomNavigationBarItem(
      icon: SvgPicture.asset(
        path,
        height: 13.sp,
        color: isSelected ? MyTheme.primaryColor : null,
      ),
      label: title,
    );
  }
}
