import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nano_test/presentations/componenets/decoration/app_decoration.dart';
import 'package:nano_test/presentations/splash/bloc/splash_bloc.dart';
import 'package:nano_test/presentations/splash/bloc/splash_event.dart';
import 'package:nano_test/presentations/splash/bloc/splash_state.dart';
import 'package:nano_test/utilities/injection_container.dart';
import 'package:nano_test/utilities/my_theme.dart';
import 'package:nano_test/utilities/route_generator.dart';

import '../componenets/logo_widget.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final _bloc=sl<SplashBloc>();
  @override
  void initState() {
    _bloc.add(InitialSplashEvent());
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return BlocListener<SplashBloc,SplashState>(
      bloc: _bloc,
      listener: (context,state){
        if(state is LoginSplashState){
          Navigator.of(context).pushNamedAndRemoveUntil(RouteNames.loginScreen, (route) => false);
        }else if(state is HomeSplashState){
          Navigator.of(context).pushNamedAndRemoveUntil(RouteNames.mainScreen, (route) => false);
        }
      },
      child: Container(
        decoration: AppDecoration.gradient,
        child: const Center(child: LogoWidget()),
      ),
    );
  }
}
