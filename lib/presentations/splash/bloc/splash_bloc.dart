import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nano_test/core/repositories/splash_repository.dart';
import 'package:nano_test/presentations/splash/bloc/splash_event.dart';
import 'package:nano_test/presentations/splash/bloc/splash_state.dart';

class SplashBloc extends Bloc<SplashEvent,SplashState>{
  SplashBloc():super(InitialSplashState()){
   on<InitialSplashEvent>(_onInitialSplashEvent);
  }
  SplashRepository splashRepository=SplashRepository();
  Future<void> _onInitialSplashEvent(
      InitialSplashEvent event,Emitter<SplashState>emit)async{
    try{
     await Future.delayed(const Duration(seconds: 2), () {});
     bool isLoggedIn=splashRepository.checkUserLoggedIn();
     if(isLoggedIn){
       emit(HomeSplashState());
     }else{
       emit(LoginSplashState());
     }
    }catch(e){
      emit(FailureSplashState(error: e.toString().substring(11)));
    }
  }

}