import 'package:equatable/equatable.dart';

abstract class SplashState extends Equatable{
  @override
  List<Object?> get props =>[];
}
class InitialSplashState extends SplashState{}
class LoadingSplashState extends SplashState{}
class FailureSplashState extends SplashState{
  final String error;

  FailureSplashState({required this.error});

}
class LoginSplashState extends SplashState{}
class HomeSplashState extends SplashState{}