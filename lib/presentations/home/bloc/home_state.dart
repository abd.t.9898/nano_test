import 'package:equatable/equatable.dart';
import 'package:nano_test/core/models/home/product_model.dart';

abstract class HomeState extends Equatable {
  @override
  List<Object?> get props => [];
}

class InitialHomeState extends HomeState {}

class LoadingHomeState extends HomeState {}

class SuccessHomeState extends HomeState {
  final List<ProductModel> products;

  SuccessHomeState({required this.products});
}

class FailureHomeState extends HomeState {
  final String error;

  FailureHomeState({required this.error});
}
