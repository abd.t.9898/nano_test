import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nano_test/core/models/home/product_model.dart';
import 'package:nano_test/core/repositories/home_repository.dart';
import 'package:nano_test/presentations/home/bloc/home_event.dart';
import 'package:nano_test/presentations/home/bloc/home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc() : super(InitialHomeState()) {
    on<InitialHomeEvent>(_onInitialHomeEvent);
  }

  HomeRepository homeRepository = HomeRepository();

  Future<void> _onInitialHomeEvent(
      InitialHomeEvent event, Emitter<HomeState> emit) async {
    try {
      emit(LoadingHomeState());
      List<ProductModel> products =
          await homeRepository.getProductsHomeRepository();
      emit(SuccessHomeState(products: products));
    } catch (e) {
      emit(FailureHomeState(error: e.toString().substring(11)));
    }
  }
}
