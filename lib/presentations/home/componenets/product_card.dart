import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:nano_test/core/models/home/product_model.dart';
import 'package:nano_test/presentations/componenets/my_cached_image.dart';
import 'package:nano_test/utilities/route_generator.dart';
import 'package:sizer/sizer.dart';

class ProductCard extends StatelessWidget {
  final ProductModel product;

  const ProductCard({required this.product, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.pushNamed(context, RouteNames.productScreen,arguments: product.id);
      },
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 5.w, vertical: 2.h),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            MyCachedImage(
                height: 25.h,
                width: 100.w,
                image: product.image,
                boxFit: BoxFit.contain,
                borderRadius: 10,
                child: Padding(
                  padding:  EdgeInsets.symmetric(horizontal: 3.w),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      RatingBar.builder(
                        ignoreGestures: true,
                        initialRating: product.rating.rate.toDouble(),
                        minRating: 1,
                        direction: Axis.horizontal,
                        allowHalfRating: true,
                        itemCount: 5,
                        itemSize: 15.sp,
                        itemPadding: EdgeInsets.symmetric(horizontal: 0),
                        itemBuilder: (context, _) => Icon(
                          Icons.star,
                          color: Colors.amber,
                        ),
                        onRatingUpdate: (rating) {},
                      ),
                      Text(product.price.toString()+' AED',style: Theme.of(context).textTheme.headlineLarge!.copyWith(fontSize: 16.sp),)
                    ],
                  ),
                )),
            Padding(
              padding:  EdgeInsets.symmetric(vertical: 1.h),
              child: Text(product.title,style: Theme.of(context).textTheme.labelSmall!.copyWith(fontSize: 12),),
            ),
            Text(product.description,style: Theme.of(context).textTheme.labelSmall!.copyWith(fontSize: 10,fontWeight: FontWeight.bold),),
            SizedBox(height: 1.h,),
            Divider(thickness: 0.7,),
          ],
        ),
      ),
    );
  }
}
