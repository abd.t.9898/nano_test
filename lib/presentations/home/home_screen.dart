import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:localize_and_translate/localize_and_translate.dart';
import 'package:nano_test/presentations/componenets/appbar_widget.dart';
import 'package:nano_test/presentations/componenets/error_press_widget.dart';
import 'package:nano_test/presentations/componenets/loading_widget.dart';
import 'package:nano_test/presentations/home/bloc/home_bloc.dart';
import 'package:nano_test/presentations/home/bloc/home_state.dart';
import 'package:nano_test/presentations/home/componenets/product_card.dart';
import 'package:nano_test/utilities/injection_container.dart';
import 'package:nano_test/utilities/my_theme.dart';
import 'package:sizer/sizer.dart';

import '../componenets/button_widget.dart';
import 'bloc/home_event.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>with AutomaticKeepAliveClientMixin {
  final _bloc=sl<HomeBloc>();
  @override
  void initState() {
    _bloc.add(InitialHomeEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(title: 'allProducts',height: 10.h,),
      body: BlocBuilder<HomeBloc,HomeState>(
        bloc:_bloc,
        builder:(context,state){
          if(state is LoadingHomeState){
            return const LoadingWidget();
          }else if(state is SuccessHomeState) {
            return ListView.builder(
              itemCount: state.products.length,
                itemBuilder: (context,index){
                  return ProductCard(product: state.products[index],);
                });
          }else if(state is FailureHomeState){
            return ErrorPressWidget(message:state.error,onPress:(){_bloc.add(InitialHomeEvent());});
          }else {
            return const SizedBox();
          }
        }
      ),
    );
  }

  @override
  bool get wantKeepAlive =>true;
}
