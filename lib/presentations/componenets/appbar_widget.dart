import 'package:flutter/material.dart';
import 'package:localize_and_translate/localize_and_translate.dart';
import 'package:sizer/sizer.dart';

class AppBarWidget extends StatelessWidget implements PreferredSizeWidget{
 final double height;
 final String title;
  const AppBarWidget({this.height=70,this.title='',Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
          borderRadius: BorderRadius.vertical(bottom:Radius.circular(30)),
        boxShadow: [
          BoxShadow(
              color: Colors.grey,
            blurRadius: 5,
            spreadRadius: 1,
          ),
        ],
      ),
      child: Center(child: Text(title.tr(),style: Theme.of(context).textTheme.headlineMedium,)),
    );
  }

  @override
  Size get preferredSize => Size(100.w,height);
}
