import 'package:flutter/material.dart';
import 'package:localize_and_translate/localize_and_translate.dart';
import 'package:nano_test/presentations/componenets/button_widget.dart';

class ErrorPressWidget extends StatelessWidget {
  final VoidCallback onPress;
  final String message;
  const ErrorPressWidget({required this.message,required this.onPress,Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(message),
        ButtonWidget(
          onPress: (){},
          message: 'reload'.tr(),
        ),
      ],
    );
  }
}
