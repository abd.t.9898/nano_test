import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:nano_test/presentations/componenets/loading_widget.dart';
import 'package:nano_test/utilities/my_theme.dart';
import 'package:sizer/sizer.dart';

class MyCachedImage extends StatelessWidget {
  final String image;
  final double? height;
  final double? width;
  final double darkenRatio;
  final double borderRadius;
  final Widget? child;
  final BoxFit boxFit;
  final bool withoutError;
  const MyCachedImage({required this.image,this.child,this.withoutError=false,this.boxFit=BoxFit.cover,this.borderRadius=5,this.darkenRatio=0,Key? key,  this.height, this.width}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      height: height,
      width: width,
      imageUrl: image,
      imageBuilder:
          (context, imageProvider) =>
              /*Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(borderRadius),
                ),
                child: ShaderMask(
                  child: Image(
                    image: imageProvider,
                    height: height,
                    width: width,
                    fit: boxFit,
                  ),
                  shaderCallback: (Rect bounds) {
                    return LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [Colors.transparent, MyTheme.darkBlueColor.withOpacity(0.5)],
                      stops: [
                        0.0,
                        0.9,
                      ],
                      tileMode: TileMode.mirror,
                    ).createShader(bounds);
                  },
                  blendMode: BlendMode.darken,
                ),
              ),*/
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(borderRadius),

              image: DecorationImage(
                  image: imageProvider,
                  fit: boxFit,
                  colorFilter:
                  ColorFilter.mode(Colors.black.withOpacity(darkenRatio), BlendMode.darken)),
            ),
            child: child,
          ),
      placeholder: (context, url) =>
      const LoadingWidget(),
      errorWidget: withoutError?null:(context, url, error)
      =>  Icon(Icons.error,size: 14.sp,),
    );
  }
}
