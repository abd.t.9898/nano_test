// ignore_for_file: must_be_immutable
import 'package:flutter/material.dart';
import 'package:localize_and_translate/localize_and_translate.dart';
import 'package:sizer/sizer.dart';

class ButtonWidget extends StatelessWidget {
  String message;
  VoidCallback onPress;
  double radius;

  ButtonWidget(
      {required this.message, this.radius = 20, required this.onPress, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPress,
      style: ElevatedButton.styleFrom(
        elevation: 0,
        backgroundColor: Theme.of(context).primaryColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(radius),
          side: BorderSide(color: Theme.of(context).primaryColor),
        ),
        foregroundColor: Colors.white,
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 2.h),
        child: Text(
           message.tr(),
          style: Theme.of(context).textTheme.labelSmall!.copyWith(color: Colors.white),
        ),
      ),
    );
  }
}
