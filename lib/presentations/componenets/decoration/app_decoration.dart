import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:localize_and_translate/localize_and_translate.dart';
import 'package:sizer/sizer.dart';

import '../../../utilities/my_theme.dart';

class AppDecoration{
  static BoxDecoration gradient=BoxDecoration(
      gradient: LinearGradient(
          colors: [
            MyTheme.secondaryColor,
            MyTheme.primaryColor,
          ]
      )
  );
  static BoxDecoration buttonDecoration=BoxDecoration(
    color: Colors.white,
    boxShadow: [
      BoxShadow(
        color: Colors.grey,
        blurRadius: 1,
        spreadRadius: 0,
      ),
    ],
    borderRadius: BorderRadius.circular(10),
  );

  static InputDecoration emailInputDecoration({required BuildContext context,bool isValid=false}) =>
      InputDecoration(
          suffixIcon: Icon(
            Icons.check_circle_outline,
            size: 14.sp,
            color: isValid?Theme.of(context).primaryColor:Colors.grey.shade400,
          ),
        hintText: 'example@gmail.com',
        border: UnderlineInputBorder(borderSide: BorderSide(color: Colors.grey.shade400)),
        focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.grey.shade400)),
        disabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.grey.shade400)),
        focusedErrorBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.grey.shade400)),
      );

  static InputDecoration passwordInputDecoration({required BuildContext context,required VoidCallback onPress}) =>
      InputDecoration(
        hintText: '*********',
          suffixIcon: IconButton(
            onPressed: onPress,
            padding: EdgeInsets.zero,
            icon:Icon(Icons.remove_red_eye_outlined,
              size: 14.sp,),
            color: Colors.grey.shade400,
          ),
        border: UnderlineInputBorder(borderSide: BorderSide(color: Colors.grey.shade400)),
        focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.grey.shade400)),
        disabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.grey.shade400)),
        focusedErrorBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.grey.shade400)),
      );

}