// ignore_for_file: prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:localize_and_translate/localize_and_translate.dart';
import 'package:nano_test/presentations/componenets/decoration/app_decoration.dart';
import 'package:nano_test/utilities/formatted.dart';
import '../../../utilities/my_theme.dart';

class PasswordTextField extends StatefulWidget {
  final passwordKey;

  //final controller;

  const PasswordTextField(
      {required this.passwordKey,
      //required this.controller,
      Key? key})
      : super(key: key);

  @override
  State<PasswordTextField> createState() => _PasswordTextFieldState();
}

class _PasswordTextFieldState extends State<PasswordTextField> {
  bool isShowing = true;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      key: widget.passwordKey,
      keyboardType: TextInputType.text,
      obscureText: isShowing,
      textInputAction: TextInputAction.next,
      style: Theme.of(context).textTheme.labelSmall,
      inputFormatters: [
        LengthLimitingTextInputFormatter(100),
      ],
      decoration: AppDecoration.passwordInputDecoration(
          context: context,
          onPress: () {
            setState(() {
              isShowing = !isShowing;
            });
          }),
      onSaved: (val) {},
      validator: (val) {
        if(val==null||val==''){
          return 'passwordError'.tr();
        }else{
          return null;
        }
      },
      onFieldSubmitted: (val) {
        widget.passwordKey.currentState!.validate();
      },
      onChanged: (val) {
        widget.passwordKey.currentState!.validate();
      },
    );
  }
}
