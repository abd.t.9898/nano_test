// ignore_for_file: prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:localize_and_translate/localize_and_translate.dart';
import 'package:nano_test/presentations/componenets/decoration/app_decoration.dart';
import 'package:nano_test/utilities/formatted.dart';
import '../../../utilities/my_theme.dart';


class EmailTextField extends StatefulWidget {
  final emailKey;
  //final controller;

  const EmailTextField(
      {required this.emailKey,
        //required this.controller,
        Key? key})
      : super(key: key);

  @override
  State<EmailTextField> createState() => _EmailTextFieldState();
}

class _EmailTextFieldState extends State<EmailTextField> {
  bool isValid=false;
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      key: widget.emailKey,
      keyboardType: TextInputType.emailAddress,
      textInputAction: TextInputAction.next,
      style: Theme.of(context).textTheme.labelSmall,
      inputFormatters: [
        LengthLimitingTextInputFormatter(100),
      ],
      decoration: AppDecoration.emailInputDecoration(context: context,isValid: isValid),
      onSaved: (val) {},
      validator: (val) {
        //String? error= isEmailValid(val);
        if(val==null||val==''){
         return 'emailError'.tr();
        }
      },
      onFieldSubmitted: (val) {
        widget.emailKey.currentState!.validate();
      },
      onChanged: (val) {
        widget.emailKey.currentState!.validate();
      },
    );
  }
}
