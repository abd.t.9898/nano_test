import 'package:flutter/material.dart';
import 'package:localize_and_translate/localize_and_translate.dart';
import 'package:nano_test/presentations/auth/componenets/email_text_field.dart';
import 'package:nano_test/presentations/auth/componenets/password_text_field.dart';
import 'package:nano_test/presentations/auth/login/bloc/login_bloc.dart';
import 'package:nano_test/presentations/auth/login/bloc/login_event.dart';
import 'package:nano_test/presentations/auth/login/bloc/login_state.dart';
import 'package:nano_test/presentations/componenets/button_widget.dart';
import 'package:nano_test/presentations/componenets/decoration/app_decoration.dart';
import 'package:nano_test/presentations/componenets/logo_widget.dart';
import 'package:nano_test/utilities/injection_container.dart';
import 'package:nano_test/utilities/route_generator.dart';
import 'package:sizer/sizer.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late GlobalKey<FormFieldState> emailKey;
  late GlobalKey<FormFieldState> passwordKey;
  late GlobalKey<FormState> formKey;
  final _bloc = sl<LoginBloc>();

  @override
  void initState() {
    emailKey = GlobalKey<FormFieldState>();
    passwordKey = GlobalKey<FormFieldState>();
    formKey = GlobalKey<FormState>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: Container(
              width: 100.w,
              decoration: AppDecoration.gradient,
              padding: EdgeInsets.symmetric(vertical: 2.h, horizontal: 5.w),
              child: SingleChildScrollView(
                physics: NeverScrollableScrollPhysics(),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 15.w, vertical: 3.h),
                      child: const LogoWidget(),
                    ),
                    SizedBox(
                      height: 5.h,
                    ),
                    Text(
                      'logIn'.tr(),
                      style: Theme.of(context).textTheme.headlineLarge,
                    ),
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 5.w, vertical: 1.h),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Form(
                        key: formKey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'email'.tr(),
                              style: Theme.of(context).textTheme.labelSmall,
                            ),
                            EmailTextField(
                              emailKey: emailKey,
                            ),
                            SizedBox(
                              height: 5.h,
                            ),
                            Text(
                              'password'.tr(),
                              style: Theme.of(context).textTheme.labelSmall,
                            ),
                            PasswordTextField(
                              passwordKey: passwordKey,
                            ),
                            SizedBox(
                              height: 4.h,
                            ),
                          ],
                        )),
                    BlocListener<LoginBloc, LoginState>(
                      bloc: _bloc,
                      listener: (context, state) {
                        if (state is SuccessLoginState) {
                          Navigator.of(context).pushNamedAndRemoveUntil(
                              RouteNames.mainScreen, (route) => false);
                        }
                      },
                      child: Row(
                        children: [
                          Expanded(
                            child: ButtonWidget(
                              onPress: () {
                                if (formKey.currentState!.validate()) {
                                  _bloc.add(PressLoginEvent(
                                      email: emailKey.currentState!.value,
                                      password:
                                          passwordKey.currentState!.value));
                                }
                              },
                              message: 'continue',
                            ),
                          ),
                        ],
                      ),
                    ),
                    Center(
                      child: TextButton(
                        onPressed: () {},
                        child: Text(
                          'needHelp'.tr(),
                          style: Theme.of(context).textTheme.labelSmall,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
