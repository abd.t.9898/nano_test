import 'package:equatable/equatable.dart';

abstract class LoginEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class PressLoginEvent extends LoginEvent {
  final String email;
  final String password;

  PressLoginEvent({
    required this.email,
    required this.password,
  });
}
