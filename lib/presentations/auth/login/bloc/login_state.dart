import 'package:equatable/equatable.dart';

abstract class LoginState extends Equatable {
  @override
  List<Object?> get props => [];
}
class InitialLoginState extends LoginState{}
class SuccessLoginState extends LoginState{}