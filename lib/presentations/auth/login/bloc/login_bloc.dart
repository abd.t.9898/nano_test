import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:localize_and_translate/localize_and_translate.dart';
import 'package:nano_test/core/repositories/auth_repository.dart';
import 'package:nano_test/presentations/auth/login/bloc/login_event.dart';
import 'package:nano_test/presentations/auth/login/bloc/login_state.dart';
import 'package:nano_test/utilities/dialogs.dart';

class LoginBloc extends Bloc<LoginEvent,LoginState>{
  LoginBloc():super(InitialLoginState()){
    on<PressLoginEvent>(_onPressLoginEvent);
  }
  AuthRepository authRepository=AuthRepository();
  Future<void> _onPressLoginEvent(PressLoginEvent event,Emitter<LoginState>emit)async{
    try{
      Dialogs.loadingDialog();
      await authRepository.loginAuthRepository(event: event);
      emit(SuccessLoginState());
      Dialogs.successDialog(message: 'loginSuccessfully'.tr());
    }catch(e){
      Dialogs.failureDialog(message: e.toString().substring(11));
    }
  }
}