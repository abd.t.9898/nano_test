import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:localize_and_translate/localize_and_translate.dart';
import 'package:nano_test/presentations/componenets/button_widget.dart';
import 'package:nano_test/presentations/componenets/decoration/app_decoration.dart';
import 'package:nano_test/presentations/componenets/error_press_widget.dart';
import 'package:nano_test/presentations/componenets/my_cached_image.dart';
import 'package:nano_test/presentations/product/bloc/product_bloc.dart';
import 'package:nano_test/presentations/product/bloc/product_event.dart';
import 'package:nano_test/presentations/product/bloc/product_state.dart';
import 'package:nano_test/utilities/injection_container.dart';
import 'package:nano_test/utilities/my_theme.dart';
import 'package:sizer/sizer.dart';

import '../componenets/loading_widget.dart';

class ProductScreen extends StatefulWidget {
  final int id;

  const ProductScreen({required this.id, Key? key}) : super(key: key);

  @override
  State<ProductScreen> createState() => _ProductScreenState();
}

class _ProductScreenState extends State<ProductScreen> {
  final _bloc = sl<ProductBloc>();

  @override
  void initState() {
    _bloc.add(InitialProductEvent(id: widget.id));
    super.initState();
  }

  final double _headerHeight = 33.h;
  final double _maxHeight = 50.h;
  bool _isDragUp = true;
  double _bodyHeight = 0.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        //elevation: 1,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Container(
            margin: EdgeInsets.all(8.sp),
            decoration: AppDecoration.buttonDecoration,
            child: Icon(
              Icons.arrow_back_rounded,
              color: Colors.black,
              size: 15.sp,
            ),
          ),
        ),
        actions: [
          GestureDetector(
            onTap: () {
              // Navigator.pop(context);
            },
            child: Container(
              margin: EdgeInsets.all(8.sp),
              padding: EdgeInsets.symmetric(horizontal: 2.5.w),
              decoration: AppDecoration.buttonDecoration,
              child: Icon(
                Icons.more_vert,
                color: Colors.black,
                size: 15.sp,
              ),
            ),
          ),
        ],
      ),
      body: BlocBuilder<ProductBloc, ProductState>(
          bloc: _bloc,
          builder: (context, state) {
            if (state is LoadingProductState) {
              return const LoadingWidget();
            } else if (state is SuccessProductState) {
              return SafeArea(
                child: Scaffold(
                  body: MyCachedImage(
                    image: state.product.image,
                    height: 55.h,
                    width: 100.w,
                    boxFit: BoxFit.contain,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 5.w),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'detail'.tr(),
                            style: Theme.of(context)
                                .textTheme
                                .headlineSmall!
                                .copyWith(color: MyTheme.darkBlueColor),
                          ),
                          Column(
                            children: [
                              Text(
                                state.product.price.toString(),
                                style: Theme.of(context)
                                    .textTheme
                                    .headlineSmall!
                                    .copyWith(color: MyTheme.darkBlueColor),
                              ),
                              SizedBox(height: 5.h,),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                  bottomSheet: AnimatedContainer(
                      constraints: BoxConstraints(
                        maxHeight: _maxHeight,
                        minHeight: _headerHeight,
                      ),
                      curve: Curves.easeOut,
                      height: _bodyHeight,
                      duration: const Duration(milliseconds: 300),
                      child: GestureDetector(
                        onVerticalDragUpdate: _onVerticalDragUpdate,
                        onVerticalDragEnd: _onVerticalDragEnd,
                        child: Column(
                          children: <Widget>[
                            Container(
                              width: 100.w,
                              height: _headerHeight,
                              padding: EdgeInsets.symmetric(horizontal: 5.w,vertical: 2.h),
                              alignment: Alignment.center,
                              decoration: const BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(20.0),
                                    topLeft: Radius.circular(20.0),
                                  ),
                                  boxShadow: <BoxShadow>[
                                    BoxShadow(
                                        color: Colors.grey,
                                        spreadRadius: 0.0,
                                        blurRadius: 1.0),
                                  ]),
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      Container(
                                        decoration:
                                            AppDecoration.buttonDecoration,
                                        height: 6.h,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 3.w),
                                        child: Icon(
                                          Icons.ios_share,
                                          color: Colors.black,
                                          size: 15.sp,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 2.w,
                                      ),
                                      Expanded(
                                          child: ButtonWidget(
                                        message: 'orderNow'.tr(),
                                        onPress: () {},
                                      ))
                                    ],
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding:
                                          EdgeInsets.symmetric(vertical: 1.h),
                                      child: SingleChildScrollView(
                                        child: Column(
                                          children: [
                                            Text(
                                              state.product.description,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .labelSmall!
                                                  .copyWith(
                                                      fontSize: 10,
                                                      fontWeight: FontWeight.bold),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Expanded(
                              child: Container(
                                width: 100.w,
                                color: Colors.white,
                                child: Container(
                                  width: 100.w,
                                  margin: EdgeInsets.symmetric(horizontal: 5.w,vertical: 1.h),
                                  padding: EdgeInsets.symmetric(horizontal: 3.w,vertical: 1.h),
                                  color: Colors.grey.shade300,
                                  alignment: Alignment.center,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                    children: [
                                      Row(
                                        children: [
                                          Text('reviews'.tr(),style: Theme.of(context).textTheme.labelSmall,),
                                          Text('(${state.product.rating.count})',style: Theme.of(context).textTheme.labelSmall,),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                                        children: [
                                          Text('${state.product.rating.rate}',style: Theme.of(context).textTheme.labelSmall,),
                                          RatingBar.builder(
                                            ignoreGestures: true,
                                            initialRating: state.product.rating.rate.toDouble(),
                                            minRating: 1,
                                            direction: Axis.horizontal,
                                            allowHalfRating: true,
                                            itemCount: 5,
                                            itemSize: 25.sp,
                                            itemPadding: EdgeInsets.symmetric(horizontal: 0),
                                            itemBuilder: (context, _) => Icon(
                                              Icons.star,
                                              color: Colors.amber,
                                            ),
                                            onRatingUpdate: (rating) {},
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      )),
                ),
              );
            } else if (state is FailureProductState) {
              return ErrorPressWidget(
                  message: state.error,
                  onPress: () {
                    _bloc.add(InitialProductEvent(id: widget.id));
                  });
            } else {
              return const SizedBox();
            }
          }),
    );
  }

  void _onVerticalDragEnd(DragEndDetails details) {
    if (_isDragUp) {
      _isDragUp = false;
    } else {
      _isDragUp = true;
    }
    setState(() {});
  }

  void _onVerticalDragUpdate(DragUpdateDetails data) {
    double _draggedAmount = 100.h - data.globalPosition.dy;
    if (_isDragUp) {
      if (_draggedAmount < 100.0) _bodyHeight = _draggedAmount;
      if (_draggedAmount > 100.0) _bodyHeight = _maxHeight;
    } else {
      double _downDragged = _maxHeight - _draggedAmount;
      if (_downDragged < 100.0) _bodyHeight = _draggedAmount;
      if (_downDragged > 100.0) _bodyHeight = 0.0;
    }
    setState(() {});
  }
}
