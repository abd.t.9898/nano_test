import 'package:equatable/equatable.dart';
import 'package:nano_test/core/models/home/product_model.dart';

abstract class ProductState extends Equatable {
  @override
  List<Object?> get props => [];
}

class InitialProductState extends ProductState {}

class LoadingProductState extends ProductState {}

class SuccessProductState extends ProductState {
  final ProductModel product;

  SuccessProductState({required this.product});
}

class FailureProductState extends ProductState {
  final String error;

  FailureProductState({required this.error});
}
