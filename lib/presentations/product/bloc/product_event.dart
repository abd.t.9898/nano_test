import 'package:equatable/equatable.dart';

abstract class ProductEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class InitialProductEvent extends ProductEvent {
  final int id;
  InitialProductEvent({required this.id});
}
