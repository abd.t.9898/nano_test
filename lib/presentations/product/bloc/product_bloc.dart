import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nano_test/core/models/home/product_model.dart';
import 'package:nano_test/core/repositories/home_repository.dart';
import 'package:nano_test/presentations/product/bloc/product_event.dart';
import 'package:nano_test/presentations/product/bloc/product_state.dart';

class ProductBloc extends Bloc<ProductEvent, ProductState> {
  ProductBloc() : super(InitialProductState()) {
    on<InitialProductEvent>(_onInitialProductEvent);
  }

  HomeRepository homeRepository = HomeRepository();

  Future<void> _onInitialProductEvent(
      InitialProductEvent event, Emitter<ProductState> emit) async {
    try {
      emit(LoadingProductState());
      ProductModel product =
          await homeRepository.getProductHomeRepository(id: event.id);
      emit(SuccessProductState(product: product));
    } catch (e) {
      emit(FailureProductState(error: e.toString().substring(11)));
    }
  }
}
