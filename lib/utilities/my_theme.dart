import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sizer/sizer.dart';

class MyTheme{

  static Color primaryColor=const Color(0xff2AB3C6);
  static Color secondaryColor=const Color(0xff188095);
  static Color darkBlueColor=const Color(0xff08293B);
  
  static TextStyle textStyle({ Color color=Colors.white , double fontSize=11,FontWeight fontWeight=FontWeight.w400}) {
    return GoogleFonts.openSans(color:color ,fontSize: fontSize.sp,fontWeight:  fontWeight);
  }
}