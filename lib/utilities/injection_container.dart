import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:localize_and_translate/localize_and_translate.dart';
import 'package:nano_test/presentations/auth/login/bloc/login_bloc.dart';
import 'package:nano_test/presentations/home/bloc/home_bloc.dart';
import 'package:nano_test/presentations/product/bloc/product_bloc.dart';
import 'package:nano_test/presentations/splash/bloc/splash_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

final sl = GetIt.instance;
BaseOptions options = BaseOptions(
  connectTimeout: 10000,
  receiveTimeout: 10000,
  sendTimeout: 10000,
  responseType: ResponseType.json,
);
final myDio = Dio(options);
late final SharedPreferences prefs;

Future<void> initApp() async {
  prefs = await SharedPreferences.getInstance();

  await translator.init(
    localeType: LocalizationDefaultType.device,
    languagesList: <String>['ar', 'en'],
    assetsDirectory: 'assets/langs/',
  );

  sl.registerFactory(() => SplashBloc());
  sl.registerFactory(() => LoginBloc());
  sl.registerFactory(() => HomeBloc());
  sl.registerFactory(() => ProductBloc());

}
