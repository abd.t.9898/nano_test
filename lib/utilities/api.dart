class Api {

  static String host = "https://fakestoreapi.com";

  /// auth
  static String login = "$host/auth/login";

  /// main
  static String getProducts = "$host/products";
  static String getProduct({required int id}) => "$host/products/$id";

}

