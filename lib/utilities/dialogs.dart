import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:localize_and_translate/localize_and_translate.dart';
import 'package:nano_test/utilities/my_theme.dart';


class Dialogs {
  static void successDialog(
      {required String message,
        IconData iconData = Icons.check_circle_outline}) {
    EasyLoading.instance
      ..displayDuration = const Duration(milliseconds: 3000)
      ..indicatorType = EasyLoadingIndicatorType.foldingCube
      ..loadingStyle = EasyLoadingStyle.custom
      ..indicatorSize = 30.0
      ..successWidget = Icon(
        iconData,
        color: MyTheme.primaryColor,
      )
      ..radius = 10.0
      ..backgroundColor = Colors.white.withOpacity(0.8)
      ..indicatorColor = MyTheme.primaryColor
      ..indicatorColor = MyTheme.primaryColor
      ..textColor = Colors.black
      ..maskColor = Colors.white.withOpacity(0.5)
      ..userInteractions = false
      ..maskType = EasyLoadingMaskType.none
      ..dismissOnTap = true;

    EasyLoading.showSuccess(message.tr());
  }

  static void failureDialog(
      {required String message,
        IconData iconData = Icons.error_outline_sharp}) {
    EasyLoading.instance
      ..displayDuration = const Duration(milliseconds: 5000)
      ..indicatorType = EasyLoadingIndicatorType.foldingCube
      ..loadingStyle = EasyLoadingStyle.custom
      ..indicatorSize = 30.0
      ..radius = 20.0
      ..successWidget = Icon(
        iconData,
        color: Colors.red,
      )
      ..progressColor = MyTheme.primaryColor
      ..backgroundColor = Colors.white.withOpacity(.7)
      ..indicatorColor = MyTheme.primaryColor
      ..textColor = Colors.black
      ..maskColor = Colors.white.withOpacity(0.7)
      ..userInteractions = false
      ..maskType = EasyLoadingMaskType.custom
      ..dismissOnTap = true;

    EasyLoading.showSuccess(message.tr());
  }

  static void loadingDialog() {
    EasyLoading.instance
      ..displayDuration = const Duration(milliseconds: 3000)
      ..indicatorType = EasyLoadingIndicatorType.threeBounce
      ..loadingStyle = EasyLoadingStyle.custom
      ..indicatorSize = 30.0
      ..radius = 10.0
      ..boxShadow = <BoxShadow>[]
      ..backgroundColor = Colors.transparent
      ..indicatorColor = MyTheme.primaryColor
      ..textColor = Colors.black
      ..maskColor = Colors.white.withOpacity(0.5)
      ..userInteractions = false
      ..maskType = EasyLoadingMaskType.custom
      ..dismissOnTap = false;

    EasyLoading.show(status: 'loading'.tr());
  }

  static void dismissDialog() {
    EasyLoading.dismiss();
  }

}
