import 'package:localize_and_translate/localize_and_translate.dart';

String? isEmailValid(String? email) {
  if(email==null){
    return 'emailError'.tr();
  }
  bool emailValid =
  RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
      .hasMatch(email);
  if(emailValid){
    return null;
  }else{
    return 'emailError'.tr();
  }
}