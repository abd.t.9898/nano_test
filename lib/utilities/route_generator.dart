import 'package:flutter/material.dart';
import 'package:nano_test/presentations/auth/login/login_screen.dart';
import 'package:nano_test/presentations/main/main_screen.dart';
import 'package:nano_test/presentations/product/product_screen.dart';

import '../presentations/splash/splash_screen.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;

    switch (settings.name) {
      case RouteNames.splashScreen:
        return MaterialPageRoute(
          builder: (_) => const SplashScreen(),
        );
      case RouteNames.mainScreen:
        return MaterialPageRoute(
          builder: (_) => const MainScreen(),
        );
      case RouteNames.loginScreen:
        return MaterialPageRoute(
          builder: (_) => const LoginScreen(),
        );
      case RouteNames.productScreen:
        return MaterialPageRoute(
          builder: (_) =>  ProductScreen(id: args==null?0:args as int),
        );
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('Error'),
        ),
        body: const Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}

class RouteNames {
  static const splashScreen = '/';
  static const loginScreen = '/login-screen';
  static const mainScreen = '/main-screen';
  static const homeScreen = '$mainScreen/home-screen';
  static const productScreen = '$homeScreen/product-screen';
}
