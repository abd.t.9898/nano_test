import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:localize_and_translate/localize_and_translate.dart';
import 'package:nano_test/utilities/injection_container.dart';
import 'package:nano_test/utilities/my_theme.dart';
import 'package:nano_test/utilities/route_generator.dart';
import 'package:sizer/sizer.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await initApp();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]).then((_) {
    runApp(
      const MyApp(),
    );
  });

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return LocalizedApp(
      child: Sizer(builder: (buildContext, orientation, deviceType) {
        return GestureDetector(
          onTap: () => {FocusScope.of(context).requestFocus(FocusNode())},
          child: MaterialApp(
            localizationsDelegates: translator.delegates,
            locale: const Locale('en', 'US'),//translator.activeLocale,
            supportedLocales: translator.locals(),
            debugShowCheckedModeBanner: false,
            initialRoute: RouteNames.splashScreen,
            onGenerateRoute: RouteGenerator.generateRoute,
            builder: EasyLoading.init(),
            theme: ThemeData(
                scaffoldBackgroundColor: Colors.white,
                appBarTheme:  AppBarTheme(
                  elevation: 0.0,
                  color: Colors.transparent,
                  titleTextStyle:TextStyle( fontSize: 15.sp,fontStyle: FontStyle.normal),
                ),
                primaryColor: MyTheme.primaryColor,
                textTheme: TextTheme(
                  headlineLarge:MyTheme.textStyle(fontSize: 20.sp,color: Colors.white,fontWeight: FontWeight.bold),
                  headlineMedium:MyTheme.textStyle(fontSize: 17.sp,color: MyTheme.darkBlueColor,fontWeight: FontWeight.bold),
                  labelSmall:MyTheme.textStyle(fontSize: 9.sp,color: Colors.black),
                  titleSmall:MyTheme.textStyle(fontSize: 10.sp,),
                  headlineSmall:MyTheme.textStyle(fontSize: 13.sp,color: Colors.white,),
                )
            ),
            themeMode: ThemeMode.light,
          ),
        );
      }),
    );
  }
}
